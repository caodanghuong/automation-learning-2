package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;

public class Topic_12_Button_Radio_Checkbox_Alert {
	private WebDriver driver;
	private Actions action;
	private JavascriptExecutor jsExcutor;
	Alert alert;
	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  action = new Actions(driver);
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
  }
  @Test
  public void TC_01_Button() throws Exception {
	 driver.get("https://www.bhphotovideo.com/");
	 
	 // Hover computer link -> Display (Mac)
	 action.moveToElement(driver.findElement(By.xpath("//a[@data-selenium = \"computersLink\"]/span"))).perform();
	 Thread.sleep(2000);
	 //click (selenium builttin)
	 driver.findElement(By.xpath("//div[@class= \\\"category-image\\\"]/img[@alt= \\\"Mac\\\"]")).click();
	 
	 Assert.assertTrue(driver.findElement(By.xpath("//div[@class='container_2LmZG1Br6z1iEjEmzF-rDw']/h1[text()='Mac']")).isDisplayed());
	 
  }
  @Test
  public void TC_02_Default_Radio_Button_Checkbox() throws Exception {
	  driver.get("https://demos.telerik.com/kendo-ui/checkbox");
	  By DualZoneCheckbox = By.xpath("//label[text()='Dual-zone air conditioning']/preceding-sibling::input");
	  //Click to checkbox (Checked)
	  driver.findElement(DualZoneCheckbox).click();
	  Thread.sleep(3000);

	  
	  // Verify selected success 
	  Assert.assertTrue(driver.findElement(DualZoneCheckbox).isDisplayed());
	  
	  //Click to checkbox (UnChecked)
	  driver.findElement(DualZoneCheckbox).click();
	  Thread.sleep(3000);
	  
	  // Verify Unselected success 
	  Assert.assertFalse(driver.findElement(DualZoneCheckbox).isDisplayed());
  }
  @Test
  public void TC_03_Custom_Radio_Button_Checkbox() throws Exception{
	 driver.get("https://material.angular.io/components/radio/examples");
	 // không click được nhưng verify được (isSelected)
	 By summerInputRadio = By.xpath("//input[@value = 'Summer']");
	 
	 //By summerDivRadio = By.xpath("");

	 
	 //Click summer radio button 
	 //Xem lại chỗ này để sau còn làm lại. chỗ này có dùng javascript 
	 Thread.sleep(3000);
	 //Verify 
	 Assert.assertTrue(driver.findElement(summerInputRadio).isSelected());
  }
  @Test
  public void TC_04_Alert() throws Exception{
	  driver.get("http://www.google.com.vn/");
	  //Create new  Accept Alert 
	  jsExcutor.executeScript("alert ('Create a new accept alert');");
	  Thread.sleep(3000);
	  //Swith qua alert 
	  alert  = driver.switchTo().alert();
	  Assert.assertEquals(alert.getText(),"Create a new accept alert");
	  // Muốn accept 
	  alert.accept();
	  
	  //Create new  Confirm Alert 
	  jsExcutor.executeScript("confirm('Create a new confirm alert');");
	  Thread.sleep(3000);
	  //Swith qua alert 
	  alert  = driver.switchTo().alert();
	  Assert.assertEquals(alert.getText(),"Create a new confirm alert");
	  //Muốn cancel (confirm/prompt)
	  alert.dismiss();
	  
	  //Create new  prompt Alert 
	  jsExcutor.executeScript("prompt('Create a new confirm alert');");
	  Thread.sleep(3000);
	  //Swith qua alert 
	  alert  = driver.switchTo().alert();

	  alert.sendKeys("Automation Testing");
	  Thread.sleep(3000);
	  Assert.assertEquals(alert.getText(),"Create a new prompt alert");
	  alert.accept();	  
  }
  @Test
  public void TC_05_Alert() throws Exception{
	  driver.get("https://automationfc.github.io/basic-form/index.html");
	  
	  String name = "Cao dang huong";
	  
	  driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();
	  
	  alert = driver.switchTo().alert();
	  
	  //Verify title 
	  Assert.assertEquals(alert.getText(),"I am a JS prompt");
	  //sendkey
	  alert.sendKeys(name);
	  Thread.sleep(3000);
	  alert.accept();
	  
	  //Verify result
	  Assert.assertEquals(driver.findElement(By.xpath("//p[@id= 'result']")).getText(),"you entered: " + name);  
  }
  @Test
  public void TC_06_Authen_Alert() throws Exception{
	  String username = "admin";
	  String password = "admin";
	  driver.get("https:"+ username + ":"+ password +"//automationfc.github.io/basic-form/index.html");
	  
	  Assert.assertEquals(driver.findElement(By.xpath("//p[contains(text(),'Congratulations! You must have the proper credentials.'")));
	  
	  
  }
  @AfterClass
  public void afterClass() {
	  driver.quit();
	  
  }

}


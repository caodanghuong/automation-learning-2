package api;



import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Topic_06_Web_Element_Excercise {
	
	WebDriver driver;
	By emailTextboxBy =  By.id("mail");
	By educationTextAreaBy = By.id("edu");
	By ageUnder18RadioBy = By.id("under_18");
	
	
	@BeforeClass
	public void beforeClass() {
		  driver = new FirefoxDriver();
		  driver.get("https://automationfc.github.io/basic-form/index.html");

		  
	  }
	 @Test
	 public void TC_01_Check_Displayed() {
		 //Email checkbox 
		 //if(driver.findElement(emailTextboxBy).isDisplayed()) {
			 //driver.findElement(emailTextboxBy).sendKeys("Automation Testing");
		 //}
		 if(isElementDisplayed(emailTextboxBy)) {
			 sendkeyToElement(emailTextboxBy, "Automation Testing ");
		 }
		 
		 
		 //Education Textarea 
		 //if(driver.findElement(educationTextAreaBy).isDisplayed()) {
			// driver.findElement(educationTextAreaBy).sendKeys("Automation Testing");
		 //}
		 if(isElementDisplayed(educationTextAreaBy)) {
			 sendkeyToElement(educationTextAreaBy, "Automation Testing");
		 }
		 //Age under 18 radio button 
		 //if(driver.findElement(ageUnder18RadioBy).isDisplayed()) {
			 //driver.findElement(ageUnder18RadioBy).click();
		 //}
		 if(isElementDisplayed(ageUnder18RadioBy)) {
			 clickToElement(ageUnder18RadioBy);
		 }
		 

}
	 @Test
	 public void TC_02_Check_Enabled() {
		 //Email Textbox 
		 if(driver.findElement(By.id("mail")).isEnabled()) {
			 System.out.println("Email textbox is enabled");
		 } else {
			 System.out.println("Email textbox is disabled");
		 }

}
	 @Test
	 public void TC_03_Check_Selected() {

}
	 @AfterClass 
	 public void afterClass() {
		 driver.quit();
	 }
	 
	 public boolean isElementEnable(By by) {
		 WebElement element = driver.findElement(by);
		 if(element.isEnabled()) {
			 System.out.println("Element----"+ by + "----is Enable");
			 return true;
		 } else {
			 System.out.println("Element----"+ by + "----is DisEnable");
			 return false; 
		 }
	 }
	 public boolean isElementDisplayed(By by) {
		 WebElement element = driver.findElement(by);
		 if(element.isDisplayed()) {
			 System.out.println("Element----"+ by + "----is displayed");
			 return true;
		 } else {
			 System.out.println("Element----"+ by + "----is undisplayed");
			 return false; 
		 }
	 }
	 public boolean isElementSelected(By by) {
		 WebElement element = driver.findElement(by);
		 if(element.isSelected()) {
			 System.out.println("Element----"+ by + "----is Selected");
			 return true;
		 } else {
			 System.out.println("Element----"+ by + "----is De-selected");
			 return false;
		 }
	 }
	 
	 
	 
	public void sendkeyToElement(By by, String value) {
		WebElement element = driver.findElement(by);
		element.clear();
		element.sendKeys(value);
	}
	public void clickToElement(By by) {
		WebElement element = driver.findElement(by);
		element.click();
	}
}
package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Topic_05_01_Web_Browser_API_Exercise {
	WebDriver driver;
	
	
	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();

	  driver.get("http://live.demoguru99.com");
	 
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  
	  driver.manage().window().maximize();
	  
	  
  }
  @Test
  public void TC_01_CheckPageUrl() {
	  driver.findElement(By.xpath("//div[@class='footer']//a[text()=\"My Account\"]")).click();
	  
	  Assert.assertEquals(driver.getCurrentUrl(), "Customer Login");
	  
	  
	  driver.findElement(By.xpath("//span[text()= \"Create an Account\"]")).click();
	  
	  Assert.assertEquals(driver.getCurrentUrl(), "Create New Customer Account");


  }
  @Test
  public void TC_02_CheckTitle() {
driver.findElement(By.xpath("//div[@class='footer']//a[text()=\"My Account\"]")).click();
	  
	  Assert.assertEquals(driver.getTitle(), "http://live.demoguru99.com/index.php/customer/account/login/");
	  
	  
	  driver.findElement(By.xpath("//span[text()= \"Create an Account\"]")).click();
	  
	  Assert.assertEquals(driver.getTitle(), "http://live.demoguru99.com/index.php/customer/account/create/");

  }
  @Test
  public void TC_03_() {
	 
  }
  
  //Chạy cuối cùng sau tất cả các testcase
  @AfterClass
  public void afterClass() {

	  driver.quit();
  }

}
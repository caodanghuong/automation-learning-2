package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_13_User_Interactions {
	private WebDriver driver;
	Actions action;
	WebElement element;
	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  action = new Actions(driver);
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
  }
  @Test
  public void TC_01_Holver() {
	 driver.get("https://www.myntra.com/");
	 
	 element.findElement(By.xpath("//div[@class= 'Destop-navlink']//a[text()= 'Discover']"));
	 action.moveToElement(element).perform();
	 sleepInSecond(3);
	 driver.findElement(By.xpath("//a[text()='Retror']")).click();
	 
	 
	 
	 
  }
  @Test
  public void TC_02_Click_And_Hold_Block() {

	  driver.get("https://jqueryui.com/resources/demos/selectable/display-grid.html");
	  
	  List<WebElement> allItems = driver.findElements(By.xpath("//ol[@id='selectable']/li"));
	  System.out.println("Item Number : " + allItems.size());

	  // 12 item
	  //|0|1|2|3|4|....
	  action.clickAndHold(allItems.get(0)).moveToElement(allItems.get(3)).release().perform();
	  sleepInSecond(3);
	  
	  
	  List<WebElement> allItemSelected = driver.findElements(By.xpath("//ol[@id='selectable']/li[contains(@class= 'ui-selected ']"));
	  System.out.println("Item Number : " + allItemSelected.size());
	  Assert.assertEquals(allItemSelected.size(), 4);
	  // for_each 
	  for(WebElement item :allItemSelected ) {
		  System.out.println(item.getText());
	  }
	  
  }
  
  
  @Test
  public void TC_03_Click_And_Hold_Random() {

	  driver.get("https://jqueryui.com/resources/demos/selectable/display-grid.html");
	  
	  List<WebElement> allItems = driver.findElements(By.xpath("//ol[@id='selectable']/li"));
	  System.out.println("Item Number : " + allItems.size());
	  
	  //Nhấn phím 
	  action.keyDown(Keys.CONTROL).perform();
	  
	  
	  action.click(allItems.get(0));
	  action.click(allItems.get(3));
	  action.click(allItems.get(7));
	  action.click(allItems.get(9));
	  action.click(allItems.get(11));

	  //Nhã phím 
	  action.keyUp(Keys.CONTROL).perform();
	  sleepInSecond(3);
	 
	  List<WebElement> allItemSelected = driver.findElements(By.xpath("//ol[@id='selectable']/li[contains(@class= 'ui-selected ']"));
	  System.out.println("Item Number : " + allItemSelected.size());
	  Assert.assertEquals(allItemSelected.size(), 5);
	  // for_each 
	  for(WebElement item :allItemSelected ) {
		  System.out.println(item.getText());
	  }
	  
  }
  
  @Test
  public void TC_04_Double_Click() {
	  driver.get("https://automationfc.github.io/basic-form/");
	  
	  action.doubleClick(driver.findElement(By.xpath("//button[text() = 'Double click me']"))).perform();
	  sleepInSecond(3);
	  
	  Assert.assertEquals(driver.findElement(By.xpath("//p[@id = 'demo']")).getText(),"Hello Automation Guys!");
	  
  }
  
  
  @Test
  public void TC_05_Right_Click() {
	  driver.get("http://swisnl.github.io/jquery-contextmenu/demo.html");
	  
	  action.contextClick(driver.findElement(By.xpath("//span[text() = 'right click me']"))).perform();
	  sleepInSecond(2);
	  
	  action.moveToElement(driver.findElement(By.xpath("//li[contains(@class,'context-icon-quit')]"))).perform();
	  
	  Assert.assertTrue(driver.findElement(By.xpath("//li[contains(@class,'context-icon-quit') and contains(@class,'context-icon-holver')]")).isDisplayed());
	  
	  action.click(driver.findElement(By.xpath("//li[contains(@class,'context-icon-quit')]"))).perform();
	  
	  driver.switchTo().alert().accept();
	  
  }
	  
  
  @Test
  public void TC_06_Drag_And_Drop() {
	  driver.get("https://demos.telerik.com/kendo-ui/dragdrop/angular");
	  
	  WebElement sourceCircle = driver.findElement(By.xpath("//div[@id='draggable']"));
	  WebElement targetCircle = driver.findElement(By.xpath("//div[@id='droptarget']"));
	  
	  action.dragAndDrop(sourceCircle, targetCircle).perform();
	  sleepInSecond(2);
	  
	  
	  Assert.assertEquals(targetCircle.getText(), "You did great!");
	  

  }
	  
  public void sleepInSecond(long timeout) {
	  try {
		Thread.sleep(timeout*1000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
  }
  
  @AfterClass
  public void afterClass() {
	  driver.quit();
	  
  }

}

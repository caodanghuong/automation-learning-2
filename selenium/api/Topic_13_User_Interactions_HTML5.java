package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_13_User_Interactions_HTML5 {
	private WebDriver driver;
	private JavascriptExecutor javascriptExecutor;
	Actions action;
	private String ROOT_FOLDER = System.getProperty("user.dir");
	WebElement element;
	String javascriptPath = ROOT_FOLDER + "//DragAndDrop//drag_and_drop_helper.js";
	String jqueryPath = ROOT_FOLDER +  "//DragAndDrop//jquery_load_helper.js";
	
	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  javascriptExecutor = (JavascriptExecutor) driver;
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
  }
  @Test
	public void TC_07_Drag_And_Drop_JS_Jquaery() throws InterruptedException, IOException {
		driver.get("http://the-internet.herokuapp.com/drag_and_drop");

		String sourceCss = "#column-a";
		String targetCss = "#column-b";
		//get ra nội dung của file js ( gét thôi chưa dùng)
		String java_script = readJavascriptFile(javascriptPath);

		// Inject Jquery lib to site
		//String jqueryscript = readJavascriptFile(jqueryPath);
		//javascriptExecutor.executeScript(jqueryscript);

		// A to B
		java_script = java_script + "$(\"" + sourceCss + "\").simulateDragDrop({ dropTarget: \"" + targetCss + "\"});";
		javascriptExecutor.executeScript(java_script);
		sleepInSecond(3);
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='column-a']/header[text()='B']")).isDisplayed());

		// B to A
		javascriptExecutor.executeScript(java_script);
		sleepInSecond(3);
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='column-a']/header[text()='A']")).isDisplayed());
	}

	  
  public void sleepInSecond(long timeout) {
	  try {
		Thread.sleep(timeout*1000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
  }
  
  @AfterClass
  public void afterClass() {
	  driver.quit();
	  
  }

public String readJavascriptFile(String file) throws IOException {
		Charset cs = Charset.forName("UTF-8");
		FileInputStream stream = new FileInputStream(file);
		try {
			Reader reader = new BufferedReader(new InputStreamReader(stream, cs));
			StringBuilder builder = new StringBuilder();
			char[] buffer = new char[8192];
			int read;
			while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
				builder.append(buffer, 0, read);
			}
			return builder.toString();
		} finally {
			stream.close();
		}
	}

}
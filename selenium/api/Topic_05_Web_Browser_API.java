package api;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_05_Web_Browser_API {
	//1_User thao tác
	//2_Mở Browserlên
	//3_User nhập Url vào application: tiki.vn 
	//4_User tải lại trang 
	//5_User click vào 1 đường link(Web element)
	//6_User back lại vào trang trước đó 
	//7_User forward tới trang 
	//8_User get title của trang 
	//9_ User get ra Url của trang 
	//10_ User get source code của trang 
	//11_ User Đóng application/Browser 
	
	//Khai báo biến(Declare)

	WebDriver driver;
	
	

  @BeforeClass
  public void beforeClass() {
	  //Khởi tạo Firefox 
	  //2_Mở Browserlên
	  driver = new FirefoxDriver();
	  //3_User nhập Url vào application: tiki.vn 
	  driver.get("https://tiki.vn/");
	  //8_User get title của trang- Tuyệt đối 
	  String HomePageTitle = driver.getTitle();
	  Assert.assertEquals(HomePageTitle, "Tiki - Mua hàng online giá tốt, hàng chuẩn, ship nhanh");
	  
	  Assert.assertEquals(driver.getTitle(), "Tiki - Mua hàng online giá tốt, hàng chuẩn, ship nhanh");

	  
	  //9_ User get ra Url của trang 
	  Assert.assertEquals(driver.getCurrentUrl(), "https://tiki.vn/");
	  
	  
	  //10_ User get source code của trang
	  driver.getPageSource();
	  
	  //tương đối 
	  Assert.assertTrue(driver.getPageSource().contains("Phụ Kiện - Thiết Bị Số"));
	  
	  //get ID của tab hoặc windown hiện tại(*)
	  driver.getWindowHandle();
	 
	  //Dùng để chờ cho element được xuất hiện (Thao tác được)-WebdriverWait 
	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  //Chờ cho page được load xong thì thao tác 
	  driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	  //Chờ cho script được execute xong (Javascript execute)
	  driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
	  //Phóng to trình duyệt 
	  driver.manage().window().maximize();
	  //Test GUI(font/size/color...)
	  driver.manage().window().getSize();
	  //6_User back lại vào trang trước đó 
	  driver.navigate().back();
	  //4_User tải lại trang 
	  driver.navigate().refresh();
	  //7_User forward tới trang 
	  driver.navigate().forward();
	  //Sẻ có học chuyên sâu về Alert/Window(tab)/Iframe(Frame)
	  driver.switchTo().alert();
	  
	  
	  driver.switchTo().window(" ");
	  
	  driver.switchTo().frame(" ");
	  
	  //đóng trình duyệt trong trường hợp 1 tab thì giống nhau
	  // Đóng tab đang active trong trường hợp nhiều tab 
	  driver.close();
	  
	  // Đóng trình duyệt dù 1 tab hay nhiều tab 
	  driver.quit();
	  

  }
  @Test
  public void TC_01_() {
	 
  }
  @Test
  public void TC_02() {
	  
  }
  @Test
  public void TC_03_() {
	 
  }
  
 
  @AfterClass
  public void afterClass() {

	  
  }

}


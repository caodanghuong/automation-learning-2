package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Topic_09_TextBox_TextArea {
	private WebDriver driver;
	private String email, userID, password, loginPageUrl;
	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  System.setProperty("webdriver.firefox.bin","/Users/caodanghuong/Desktop/Automation Learn/02_Selenium API/BrowserDriver/Firefox 47.0.2.dmg");
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  driver.get("http://demo.guru99.com/v4");
	  
	  email = "danghuong" + randomNumber() + "@hotmail.com";
  }
  @Test
  public void TC_01_RegisterToSystem() {
	  loginPageUrl = driver.getCurrentUrl();
	  
	  driver.findElement(By.xpath("//a[Text() = 'here']")).click();
	  
	  driver.findElement(By.xpath("//input[@name= 'emailid']")).sendKeys(email);
	  
	  driver.findElement(By.xpath("//input[@name= 'btnLogin']")).click();
	  
	  userID = driver.findElement(By.xpath("//td[text() = 'User ID :']/following-sibling::td")).getText();
	  System.out.println("User ID at Register Page"+ userID);
	  
	  password = driver.findElement(By.xpath("//td[text() = 'Password :']/following-sibling::td")).getText();
	  System.out.println("Password at Register Page"+ password);
	 
  }
  @Test
  public void TC_02_LoginToSystem() {
	  //Quay lại trang cũ có 2 cách : 
	  // 1(Không nên dùng vì phụ thuộc nhiều)
	  //driver.get("http://demo.guru99.com/v4");
	  //Hoặc sử dụng cách này (2) cũng k nên dùng vì phụ thuộc nhiều 
	  //driver.navigate().back();//Register page 
	  //driver.navigate().back();//Login page 
	  // cách thứ 3
	  driver.get(loginPageUrl);
	  driver.findElement(By.xpath("//input[@name = 'uid']")).sendKeys(userID);
	  
	  driver.findElement(By.xpath("//input[@name = 'Password']")).sendKeys(password);
	  
	  driver.findElement(By.xpath("//input[@name= 'btnLogin']")).click();
	  
	  Assert.assertTrue(driver.findElement(By.xpath("//tr[@class = 'heading3']/td[text()='Manger Id : " + userID + "']")).isDisplayed());
	  // biến cục bộ 
	  String welcomeMassage = driver.findElement(By.xpath("//marquee[@class= 'heading3']")).getText();
	  Assert.assertEquals(welcomeMassage , "Welcome To Manager's Page of Guru99 Bank");
	  
  }
  @Test
  public void TC_03_NewCustomer() {
	 
  }
  @Test
  public void TC_04_EditCustomer() {
	 
  }
  
  //Chạy cuối cùng sau tất cả các testcase
  @AfterClass
  public void afterClass() {
	  driver.quit();
	  
  }
  public int randomNumber() {
	  Random rand = new Random();
	  return rand.nextInt(999999);
  }

}

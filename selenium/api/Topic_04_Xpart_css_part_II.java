package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Topic_04_Xpart_css_part_II{
	private WebDriver driver;
	By emailTextBox = By.id("email");
	By passwordTextBox = By.id("pass");
	By LoginButton = By.id("send2");
	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
  }
  @BeforeMethod
  public void runForEarchTestMethod() {
	  //Open UAT
	  driver.get("http://live.demoguru99.com/");
	  //Click vao My Account Link (Footer)
	  driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
		 	  
  }
  @Test
  public void TC_01_LoginWithEmptyEmailAndPassword() {
	 //Input vao email textbox
	 driver.findElement(emailTextBox).sendKeys("");
	//Input vao password textbox
	 driver.findElement(passwordTextBox).sendKeys("");
	 //Login button
	 driver.findElement(LoginButton).click();
	 
	 //Verify email/pass error message display
	 //Cach 1: dung ham Assert True(dieu kien)
	 //boolean status = driver.findElement(By.id("advice-required-entry-email")).getText().equals("This is a required field.");
	 //System.out.println("Status = " + status);
	 //Assert.assertTrue(status);
	 //Cach 2: Assert equals(dieu kien thuc te va mong doi)
	 Assert.assertEquals(driver.findElement(By.id("advice-required-entry-email")).getText(), "This is a required field.");
	 Assert.assertEquals(driver.findElement(By.id("advice-required-entry-pass")).getText(), "This is a required field.");

		 
		 
  }
  @Test
  public void TC_02_LoginWithInvalidEmail() {
	//Input vao email textbox
		 driver.findElement(emailTextBox).sendKeys("43532453454@243.242345");
		//Input vao password textbox
		 driver.findElement(passwordTextBox).sendKeys("");
		 //Login button
		 driver.findElement(LoginButton).click();
		 //Verify email/pass error message displayed
		 Assert.assertEquals(driver.findElement(By.id("advice-validate-email-emai")).getText(),
				 "Please enter a valid email address. For example johndoe@domain.com.");
		 Assert.assertEquals(driver.findElement(By.id("advice-required-entry-pass")).getText(),
				 "This is a required field.");

	  
  }
  @Test
  public void TC_03_LoginWithPasswordLessThan6Chart() {
	  //Input vao email textbox
	  driver.findElement(emailTextBox).sendKeys("automation@gmail.com");
	  //Input vao password textbox
	  driver.findElement(passwordTextBox).sendKeys("123");
	  //Login button
	  driver.findElement(LoginButton).click();
		 //Verify password error message displayed
		 Assert.assertEquals(driver.findElement(By.id("advice-validate-password-pass")).getText(),
				 "Please enter 6 or more characters without leading or trailing spaces.");

	 
  }
  @Test
  public void TC_04_LoginWithIncorrectPassword() {
	//Input vao email textbox
	  driver.findElement(emailTextBox).sendKeys("automation@gmail.com");
	  //Input vao password textbox
	  driver.findElement(passwordTextBox).sendKeys("etgsdgsgsgg");
	  //Login button
	  driver.findElement(LoginButton).click();
	//Verify password error message displayed
		 Assert.assertEquals(driver.findElement(By.xpath("//li[@class='error-msg']//span")).getText(),
				 "Invalid login or password.");

	 
  }
  @Test
  public void TC_05_LoginWithValidEmailAndPassword() {
	//Input vao email textbox
	driver.findElement(emailTextBox).sendKeys("automation@gmail.com");
	//Input vao password textbox
	driver.findElement(passwordTextBox).sendKeys("123123");
	//Login button
	driver.findElement(LoginButton).click();
	//Verify info the DashBoard page
	//Verify password error message displayed
	 Assert.assertEquals(driver.findElement(By.xpath("//div[@class='page-title']/h1")).getText(),
			 "MY DASHBOARD");
	 Assert.assertEquals(driver.findElement(By.xpath("//div[@class='welcome-msg']//strong")).getText(),
			 "Hello, Automation Testing!");
	 String contactInfo = driver.findElement(By.xpath("//h3[text()='Contact Information']/parent::div/following-sibling::div/p")).getText();
	 System.out.println("contactInfo = " + contactInfo);
	 Assert.assertTrue(contactInfo.contains("Automation Testing"));
	 Assert.assertTrue(contactInfo.contains("automation@gmail.com"));



	 
  }
  @Test
  public void TC_06_CreaterNewUser () {
	 
  }
  
  //Chạy cuối cùng sau tất cả các testcase
  @AfterClass
  public void afterClass() {
	  //Để tắt browes 
	  driver.quit();
	  
  }

}

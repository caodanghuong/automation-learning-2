package api;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Topic_01_Xpart_css_part_1_Locator {
	private WebDriver driver;
  @BeforeClass
  public void beforeClass() {
	  // 1 mở trình duyệt lên 
	  driver = new FirefoxDriver();
	  //Trả về 1 GUID : xxxx-xxxx-xxxx-xxx 
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  driver.get("https://m.facebook.com/");
	  
	  //Application(UAT) : Application Under Testing 
	  //Pages/ Modules 
	  //1 page/ Form: Element 
	  //Fabook Login : Email Texbox, Password texbox, login button....
	  //HTML Code: 
	  //HTML Format:
	  //1    Tagname: Thẻ HTML 
	  //2    attribute name : id/ class/ type/ data- testid/ name...
	  //3    attribute value : email/ input text login_form_box/...
	  
	  
  }
  @Test
  public void TC_01_Id() throws Exception {
	  //Tìm single element 
	  WebElement emailTextbox = driver.findElement(By.id("email"));
	  emailTextbox.clear();
	  emailTextbox.sendKeys("caodanghuongk54@gmail.com");
	  emailTextbox.isDisplayed();
	  Thread.sleep(3000);
	  // Tìm nhiều hơn 1 element 
	  //List<WebElement> Links = driver.findElements(By.tagName("a"));


  }
  @Test
  public void TC_02_Class() throws Exception {
	  driver.get("http://live.demoguru99.com/index.php/customer/account/login/");
	  driver.findElement(By.className("validate-password")).sendKeys("123456");
	  Thread.sleep(3000);
	  
  }
  @Test
  public void TC_03_Name() {
	 
  }
  @Test
  public void TC_04_() {
	  driver.get("");

  }
  @Test
  public void TC_05_LinkText() {
	  
  }
  @Test
  public void TC_06_ParticialLinkText() {
	 
  }
  @Test
  public void TC_07_Css() {
	 
  }
  @Test
  public void TC_08_Xparth() {
	 
  }
  //Chạy cuối cùng sau tất cả các testcase
  @AfterClass
  public void afterClass() {
	  //Để tắt browes 
	  driver.quit();
	  
  }

}

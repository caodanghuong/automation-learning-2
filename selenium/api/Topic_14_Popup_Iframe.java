package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_14_Popup_Iframe {
	private WebDriver driver;
	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
  }
  
  
  @Test
  public void TC_01_Popup() {
	  driver.get("https://www.javacodegeeks.com/");
	  
	  //nếu như hiển thị sẻ close popup
	  // Nếu không hiển thị thì qua step tiếp theo
	  //check popup displayed
	  if (isElementDisplayed("//div[@data-title=\"Newsletter-Books Anime Brief\"]")) {
		  driver.findElement(By.xpath("//a[text()='No Thanks!']/parent::div")).click();
		  sleepInsecond(5);
	  }
	  
	 //check popup undisplayed  
	  Assert.assertFalse(driver.findElement(By.xpath("//div[@data-title=\"Newsletter-Books Anime Brief\"]")).isDisplayed());
	 
	  driver.findElement(By.xpath("//div[@class = 'content-wrap']//input[@name= 'ulp-email']")).sendKeys("automationtest@gmail.com");
	  sleepInsecond(3);

  }
  @Test
  public void TC_02_Iframe() {
	  driver.get("https://kyna.vn/");
	  // 3 cách tìm iframe
	  //1. index
	  //driver.switchTo().frame(0);
	  //2. Iframe: id/name
	  //driver.switchTo().frame("id/name");
	  //3.WebElement (nên dùng)
	  driver.switchTo().frame(driver.findElement(By.xpath("")));
	  //đã chhuyeenr qua iframe của facebook rồi
	  
	  // Thao tác trên những element của fb
	  String kynaTextTitle = driver.findElement(By.xpath("//a[@title = 'Kyna.vn']")).getText();
	  System.out.println(kynaTextTitle);
	  driver.switchTo().defaultContent();
	  // chuyển về lại home page (kyna.vn)
	  driver.findElement(By.xpath("")).sendKeys("Java");
	  driver.findElement(By.xpath("")).click();
	  sleepInsecond(3);
	  // không cho phép nhảy từ frame này qua frame khác. luôn phaiur nhảy về defaultcontent
  }
  
  public boolean isElementDisplayed(String locator) {
	  try {
		  // 1. E lement hiễn thị và có trong DOM
		  //2.Element không hiển thị nhưng có trong DOM 
		  //3.Element không hiển thị và không  có trong DOM 
		  
		  WebElement element = driver.findElement(By.xpath(locator));
		  return element.isDisplayed();
	  } catch (Exception e) {
		  e.printStackTrace();
		  return false;
	  }
  }
  @Test
  public void TC_02_frame() {
	  
  }
  @Test
  public void TC_03_() {
	 
  }
  public void sleepInsecond(long timeout) {
	  try {
		Thread.sleep(timeout*1000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
  }
  
  @AfterClass
  public void afterClass() {
	  driver.quit();
	  
  }

}
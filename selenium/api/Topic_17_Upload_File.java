package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Topic_17_Upload_File {
	private WebDriver driver;
	private String rootFolderPath = System.getProperty("user.dir");
	
  @BeforeClass
  public void beforeClass() {
	  System.getProperty("Webdriver.gecko.driver",rootFolderPath +  "/Users/caodanghuong/Desktop/Automation Learn/02_Selenium API/BrowserDriver");
	  driver = new FirefoxDriver();
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
  }
  @Test
  public void TC_01_SendKeys() {
	 driver.get("https://blueimp.github.io/jQuery-File-Upload/");
	 
	 WebElement uploadFile = driver.findElement(By.xpath("//input[@name='files[]']"));
	 
	 uploadFile.sendKeys("/Users/caodanghuong/Desktop/Automation Learn/02_Selenium API/uploadfiles/MacPro.jpg");
	 sleepInSecond(5);
	 
  }
  @Test
  public void TC_02() {
	  
  }
  @Test
  public void TC_03_() {
	 
  }
  
  
  public void sleepInSecond(long timeout) {
	  
	  try {
		Thread.sleep(timeout*1000);
	} catch (InterruptedException e) {
				e.printStackTrace();
	}
	  
  }
  
  @AfterClass
  public void afterClass() {
	  driver.quit();
	  
  }

}

package api;



import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
public class Topic_06_Web_Element {
	//Usser thao tac vowis browser
	//Khai bao bien (Declare)
	//Instance Đại diện cho 1 thư viện/ class/nterface nào đó
	WebDriver driver;
	int price;
	double size;
	
	WebElement element;
	
	
	@BeforeClass
	public void beforeClass() {
		  driver = new FirefoxDriver();
		  // 1_ Mở browser lên
		  driver = new FirefoxDriver();
		  
	  }

	 @Test
	 public void TC_01_() {
		 //Nếu dùng 1 lần thì tương tác trực tiếp
		driver.findElement(By.xpath("")).click();
		// khai báo biến dùng được nhiều lần
		WebElement emailTexbox = driver.findElement(By.xpath("//input[@id='email']"));
		emailTexbox.clear();
		emailTexbox.sendKeys("");
		emailTexbox.isDisplayed();
		
		//Xoá dũ liệu đang có trong 1 texbox/textarea/dropdow (edittable)
		element.clear();
		
		//Nhập dữ liệu vào trong 1 texbox/textarea/dropdow (edittable)
		element.sendKeys(" ");
		
		//Đi tìm element (Số ít -1)- Nếu thấy nhiều hơn 1 thì sẻ lấy cái đầu tiên để tương tác 
		element.findElement(By.xpath(" "));
		// Đi tìm element (Số nhiều - >= 1)
		element.findElements(By.xpath(" "));
		// Tìm element và trả về duy nhất 1 
		element.findElements(By.xpath(" ")).get(0);
		element.findElement(By.xpath(" "));
		
	  }
	}



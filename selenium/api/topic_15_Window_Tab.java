package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_15_Window_Tab {
	private WebDriver driver;
	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  
	  System.out.println("ID of driver = " + driver.toString());
	  //GUI: Global Unique Identifier 
	  
  }
  @Test
  public void TC_01_Window_Tab() {
	 driver.get("https://kyna.vn/");
	 //Lấy ra ID của tab mà driver đang active ở đấy 
	 String parentWindowID = driver.getWindowHandle();
	 
	 System.out.println("Parent ID (Kyna)" + parentWindowID);
	 
	 driver.findElement(By.xpath("//img[@alt = 'android-app-icon']")).click();
	 // Switch qua google play
	 switchToWindowByTitle("Title vào đây ");
	 //switchToWindowByID(parentWindowID);	
	 // Bằng tìm điểm 
	 //Assert.assertTrue(driver.findElement(By.xpath("//button[text()='Install'")).isDisplayed());
	 // tìm bằng link
	 Assert.assertEquals(driver.getCurrentUrl(), "URL của google play");
	 
	  
	 String ChildID = driver.getWindowHandle();
	 Assert.assertEquals(driver.getCurrentUrl(), "URl của hompage");

	 System.out.println("Child  ID (Kyna - google)" + ChildID);
	 //Switch về home page
	 
	 switchToWindowByTitle("Title của page khác ");
	 //switchToWindowByID(ChildID);	 
	 Assert.assertEquals(driver.getCurrentUrl(), "URl của hompage");

	 sleepInSecond(2);
	 // apple Appstore
	 driver.findElement(By.xpath("//img[@alt = 'Apple-app-icon']")).click();


	 sleepInSecond(2);
	 
	 //switch qua app store 
	 
	 switchToWindowByTitle(" Title của page khác nữa ");
	 //switchToWindowByID(parentWindowID);
	
	 Assert.assertEquals(driver.getCurrentUrl(), "URl của Appstore ");

	 sleepInSecond(2);
	 
	 //Switch về home page
	 switchToWindowByTitle("Title của page khác ");
	 sleepInSecond(2);
	 
	 
	 driver.findElement(By.xpath("//img[@alt = 'Facebook']")).click();
	 switchToWindowByTitle("Title của facebook ");
	 Assert.assertEquals(driver.getCurrentUrl(), "URl của faceboook hay không ");


  }
  
  
  public void TC_02_Window_Tab() {
	  driver.get("http://live.demoguru99.com/index.php/");
	  driver.findElement(By.xpath("x path của nó nha soniexxperia")).click();
	  Assert.assertTrue(driver.findElement(By.xpath("    ")).getText().equals(" "));
	  
  }
  // chỉ dùng cho 2 tab/window thôi nhé
  public void switchToWindowByTitle(String parentID) {
	  // get ra tất cả các ID của cửa sổ/ tab
	  Set<String> allWindows  = driver.getWindowHandles();
	  System.out.println(allWindows);
	  // Dùng vòng lặp để duyệt qua các ID này
	  for(String id: allWindows) {
		  System.out.println("ID = " + id);
		  // ID nào khác với tham số chuyền vào (parent)
		  if(!id.equals(parentID)) {
			  
			  //Sẻ Switch qua cái ID đó
			  driver.switchTo().window(id);
			  //Thoai khỏi vongf lặp 
			  
			  break; 
		  }
	  }
  }
  
  public void switchToWindowTitle(String windowTitle) {
	  // get ra tất cả các ID của cửa sổ/ tab
	  Set<String> allWindows  = driver.getWindowHandles();
	  System.out.println(allWindows);
	  
	  // Dùng vòng lặp để duyệt qua các ID này
	  for(String id: allWindows) {
		  driver.switchTo().window(id);
		  String title = driver.getTitle();
		  if(title.equals(windowTitle)) {
			  break;
		  }
		  
	  }
  }
  
  public void closeWindowWithoutParent(String parentID) {
	  // get ra tất cả các ID của cửa sổ/ tab
	  Set<String> allWindows  = driver.getWindowHandles();
	  System.out.println(allWindows);
	  
	  // Dùng vòng lặp để duyệt qua các ID này
	  for (String id: allWindows) {
		  if(!id.equals(parentID)) {
			  driver.switchTo().window(id);
			  driver.close();
		  }
		  
		  driver.switchTo().window(parentID);
	  }
  }
  
  
  public void sleepInSecond (long timeout) {
	  try {
		Thread.sleep(timeout *1000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
  }
  
  
  
  @AfterClass
  public void afterClass() {
	  driver.quit();
	  
  }

}

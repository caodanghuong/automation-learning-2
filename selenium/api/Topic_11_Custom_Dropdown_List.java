package api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.lang.reflect.Array;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class Topic_11_Custom_Dropdown_List {
	private WebDriver driver;
	private WebDriverWait explicitWait;

	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  explicitWait = new WebDriverWait(driver,15);
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  driver.manage().window().maximize();

	  
  }
  
  @Test
  public void TC_01_JQuery() {
	  driver.get("https://jqueryui.com/resources/demos/selectmenu/default.html");
	  
	  
	  selectItemInDropdownList("//span[@id='number-button']", "//ul[@id = 'number-menu']//div", "5");
	  Assert.assertTrue(isElementDisplayed("//span[@id='number-button']//span[@class = 'ui-selectmenu-text' and text ()= '5']"));
	  
	  selectItemInDropdownList("//span[@id='number-button']", "//ul[@id = 'number-menu']//div", "15");
	  Assert.assertTrue(isElementDisplayed("//span[@id='number-button']//span[@class = 'ui-selectmenu-text' and text ()= '15']"));

	  selectItemInDropdownList("//span[@id='number-button']", "//ul[@id = 'number-menu']//div", "3");
	  Assert.assertTrue(isElementDisplayed("//span[@id='number-button']//span[@class = 'ui-selectmenu-text' and text ()= '3']"));

	  selectItemInDropdownList("//span[@id='number-button']", "//ul[@id = 'number-menu']//div", "16");
	  Assert.assertTrue(isElementDisplayed("//span[@id='number-button']//span[@class = 'ui-selectmenu-text' and text ()= '16']"));

	  

	  
	 
  }
  
  @Test
  public void TC_02_React() {
	  driver.get("https://react.semantic-ui.com/maximize/dropdown-example-selection/");
	  
	  selectItemInDropdownList("//i[@class = 'dropdown icon']","//div[@role = 'option']//span", "Elliot Fu");
	  Assert.assertTrue(isElementDisplayed("//div[@role ='listbox']/div[@class ='divider text'and text()='Elliot Fu']"));

	  selectItemInDropdownList("//i[@class = 'dropdown icon']","//div[@role = 'option']//span", "Christian");
	  Assert.assertTrue(isElementDisplayed("//div[@role ='listbox']/div[@class ='divider text'and text()='Christian']"));

	  selectItemInDropdownList("//i[@class = 'dropdown icon']","//div[@role = 'option']//span", "Justen Kitsune");
	  Assert.assertTrue(isElementDisplayed("//div[@role ='listbox']/div[@class ='divider text'and text()='Justen Kitsune']"));

  }
  
  @Test
  public void TC_03_Angular() {

	 
  }
  
  
  @AfterClass
  public void afterClass() {

	  driver.quit();
	  
	  
  }
  
  public void selectItemInDropdownList(String parentXpath, String childXpath, String childXpath1) {
	  //Click vaò 1 thẻ cha để xổ ra tất cả các thẻ con bên trong
	  driver.findElement(By.xpath(parentXpath)).click();
	  sleepInSeconds(2);
	  
	  //Lấy hết tất cả các item gán vào 1 cái list <WebElement>(FinElement)
	  List <WebElement> allItems = driver.findElements(By.xpath(childXpath1));
	  System.out.println("Item number =" + allItems.size());
	  
	  //Wait cho cacs item được laod lên thành công
	  explicitWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(childXpath1)));
	  
	  //Dùng vòng lặp để duyệt qua các item từ đầu đến cuối
	  for (int i = 0; i < allItems.size(); i ++) {
		  System.out.println("duyet lan thu = " + i);
		  //Get text của Item ra
		  
		  String itemText = allItems.get(i).getText();
		  //Kiểm tra là getText của item đó rồi so sánh với expected text mình cần click vảo 
		  if (itemText.equals(expectedItem)){
		//Nếu như thoã mãn thì click vào item đó 
			  allItems.get(i).click();
			  sleepInSeconds(2);

			  // Thoats khoir vong lap
			  break;

		  }
	  }
	  
  }
  public void sleepInSeconds(long timeout) {

	  try {
		  Thread.sleep(timeout*1000);

	  }	  catch (InterruptedException e){
		  e.printStackTrace();
		  
		  
	  }
  }
  public boolean isElementDisplayed(String xpathLocator) {
	  if(driver.findElement(By.xpath(xpathLocator)).isDisplayed()) {
		  return true;
	  } else {
		  return false;
	  }
	  
  }
}
